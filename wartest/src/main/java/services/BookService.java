package services;
import eu.tncy.jaret.Book;
import java.util.ArrayList;

public class BookService{
    private ArrayList<Book> books = new ArrayList<Book>();

    public Book addBook(Book book){
        this.books.add(book);
        return book;
    }

    public void removeBook(String bookid){
        Book book = this.getBookById(bookid);
        books.remove(book);
    }

    public Book updateBook(String bookid, Book book){
        book.setId(bookid);
        return book;
    }

    public Book getBookById(String bookid){
        for(Book book: books){
            if (book.getid().equals(bookid)){
                return book;
            }
        }
        return null;
    }

    public Book findBookByTitle(String title){
        for(Book book: books){
            if (book.gettitle()==title){
                return book;
            }
        }
        return null;
    }
}