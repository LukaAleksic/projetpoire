package eu.tncy.jaret;
import net.tncy.validator.constraints.books.ISBN;

public class Book {
    private String titre;
    private String auteur;
    @ISBN
    private String isbn;

    public Book(String title, String auteur, String isbn){
        this.titre = title;
        this.auteur = auteur;
        this.isbn = isbn;
    }

    public void setId(String id){
        this.isbn = id;
    }

    public String getid(){
        return this.isbn;
    }

    public String gettitle(){
        return this.titre;
    }
}