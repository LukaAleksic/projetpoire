 package net.tncy.validator.constraints.bank;

 import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import net.tncy.validator.constraints.bank.impl.IBANValidator;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IBANValidator.class)
@Documented
public @interface IBAN {

    String message() default "{net.tncy.validator.constraints.bank.IBAN}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean explain() default false;
}
