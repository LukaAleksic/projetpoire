package net.tncy.validator.constraints.books.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.books.ISBN;
import net.tncy.validator.util.ValidatorUtil;

/**
 * Regex : ([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)
 * @author student
 */
public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    private static final int ZERO = 0;
    private static final int TWO = 2;
    private static final int TEN = 10;
    private static final int ELEVEN = 11;
    private static final char X_CHAR = 'X';
    private static final String SEG_SEPARATOR = "-";
    private static final String EMPTY_STRING = "";
    private static final int ISBN_LEN = TEN;
    private static final int CHECK_DIGIT_POS = ISBN_LEN - 1;
    private static final Pattern PATTERN = Pattern.compile("//d{9}(//d|X)");
    private boolean explain = false;

    @Override
    public void initialize(ISBN constraintAnnotation) {
        explain = constraintAnnotation.explain();
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean valid = false;
        if (bookNumber == null || bookNumber.trim().isEmpty()) {
            valid = true;
        } else {
            String bn = bookNumber.replaceAll(SEG_SEPARATOR, EMPTY_STRING).trim();
            Matcher matcher = PATTERN.matcher(bn);
            if (!matcher.matches()) {
                explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.patternViolation");
            }
            int len = bn.length();
            if (len == ISBN_LEN) {
                char checkDigit = bn.charAt(CHECK_DIGIT_POS);
                try {
                    int checkDigitValue = computeCheckDigitValue(checkDigit);
                    int[] digits = ValidatorUtil.toDigitArray(bn.substring(ZERO, CHECK_DIGIT_POS));
                    int[] weights = ValidatorUtil.createSequence(TEN, TWO);
                    int[] products = ValidatorUtil.multiply(digits, weights);
                    int sum = ValidatorUtil.sum(products);
                    valid = checkDigitValue == (ELEVEN - (sum % ELEVEN)) % ELEVEN;
                } catch (NumberFormatException e) {
                    explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidCharacter");
                }
            } else {
                explain(constraintContext, "net.tncy.validator.constraints.book.ISBN.invalidLength");
            }
        }
        return valid;
    }

    private int computeCheckDigitValue(char c) throws NumberFormatException {
        int value = ELEVEN;
        if (Character.isDigit(c)) {
            value = Character.digit(c, TEN);
        } else if (c == X_CHAR) {
            value = TEN;
        } else {
            throw new NumberFormatException("'" + c + "' does not represent a digit.");
        }
        return value;
    }

    private void explain(ConstraintValidatorContext constraintContext, String messageKey) {
        if (explain) {
            constraintContext.buildConstraintViolationWithTemplate("{" + messageKey + "}").addConstraintViolation();
        }
    }
}
