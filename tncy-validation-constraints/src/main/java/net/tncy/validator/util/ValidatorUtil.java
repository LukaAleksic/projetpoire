package net.tncy.validator.util;

/**
 *
 * @author xavier
 */
public class ValidatorUtil {

    public static int[] toDigitArray(String s) throws NumberFormatException {
        int[] digits = new int[s.length()];
        char[] characters = s.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            char c = characters[i];
            if (Character.isDigit(c)) {
                digits[i] = Character.digit(c, 10);
            } else {
                throw new NumberFormatException("'" + c + "' does not represent a digit.");
            }
        }
        return digits;
    }

    public static int[] toDigitArray(int i) throws NumberFormatException {
        return toDigitArray(String.valueOf(i));
    }

    public static int[] createSequence(int from, int to) {
        int len = Math.abs(from - to);
        int step = (to - from) / len;
        int[] seq = new int[len + 1];
        int val = from;
        for (int i = 0; i < seq.length; i++) {
            seq[i] = val;
            val += step;
        }
        assert seq[len] == to;
        return seq;
    }

    /**
     * Multiplies every number from the source array by the factor at the same index within the weight array
     * @param source original digit array
     * @param weights factor used to multply otiginal digit
     * @return the weighted number array
     */
    public static int[] multiply(int[] source, int[] weights) {
        assert source.length == weights.length;
        int[] products = new int[source.length];
        for (int i = 0; i < products.length; i++) {
            products[i] = source[i] * weights[i];
        }
        return products;
    }

    public static int sum(int[] values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return sum;
    }

    public static int sumWithWeight(int[] values, int oddWeight, int evenWeight) {
        int sum = 0;
        for (int i = 0; i < values.length; i++) {
            if (i % 2 == 0) {
                sum += values[i] * evenWeight;
            } else {
                sum += values[i] * oddWeight;
            }
        }
        return sum;
    }

    public static int sumDigits(int value) {
        int[] digits = toDigitArray(value);
        return sum(digits);
    }

    public static int digitalize(int value) {
        int digit = 0;
        if (value < 10) {
            digit = value;
        } else {
            digitalize(sumDigits(value));
        }
        return digit;
    }
}
