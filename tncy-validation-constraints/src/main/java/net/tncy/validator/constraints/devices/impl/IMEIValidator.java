package net.tncy.validator.constraints.devices.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.devices.IMEI;
import net.tncy.validator.util.LuhnFormula;
import net.tncy.validator.util.ValidatorUtil;

/**
 * Regex : (/d{15})
 *
 * @author student
 */
public class IMEIValidator implements ConstraintValidator<IMEI, String> {

    private static final int IMEI_LEN = 15;
    private static final int ZERO = 0;
    private static final int CHECK_DIGIT_POS = IMEI_LEN - 1;
    private boolean explain = false;

    @Override
    public void initialize(IMEI constraintAnnotation) {
        explain = constraintAnnotation.explain();
    }
	 
    @Override
    public boolean isValid(String id, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean isValid = false;
        if (id == null || id.trim().isEmpty()) {
            isValid = true;
        } else {
            int len = id.length();
            if (len == IMEI_LEN) {
                try {
                    int checkDigitValue = Integer.parseInt(id.substring(CHECK_DIGIT_POS));
                    int[] digits = ValidatorUtil.toDigitArray(id.substring(ZERO, CHECK_DIGIT_POS));
                    return checkDigitValue == LuhnFormula.computeChecksumDigit(digits);
                } catch (NumberFormatException e) {
                    explain(constraintContext, "net.tncy.validator.constraints.devices.IMEI.invalidCharacter");
                }

            } else {
                explain(constraintContext, "net.tncy.validator.constraints.devices.IMEI.invalidIMEILength");
            }
        }
        return isValid;
    }

    private void explain(ConstraintValidatorContext constraintContext, String messageKey) {
        if (explain) {
            constraintContext.buildConstraintViolationWithTemplate("{" + messageKey + "}").addConstraintViolation();
        }
    }
}
